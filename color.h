#ifndef COLOR_H
#define COLOR_H

#include <iostream>

class Color
{
	protected:
		unsigned int redColor;
		unsigned int greenColor;
		unsigned int blueColor;

	public:
		// Konstruktor bezparametrowy
		Color();

		// Konstruktor przyjmujacy wartosci kolorow jako parametry
		Color(unsigned int, unsigned int, unsigned int);

		// Konstruktor kopiujacy
		Color(const Color& color);

		// Destruktor
		~Color();

		// Gettery
		unsigned int getRedColor();
		unsigned int getGreenColor();
		unsigned int getBlueColor();

		// Settery
		void setRedColor(unsigned int);
		void setGreenColor(unsigned int);
		void setBlueColor(unsigned int);

		// Operator przypisania
		Color& operator =(const Color&);
		
		// Operator porownania
		bool operator == (const Color&);

		// Operator nirownosci
		bool operator != (const Color&);

		// Operator wypisania na ekran
		friend std::ostream& operator<< (std::ostream& os, const Color&);

		// Operator wejscia, wpisuje dane do obiektu klasy Color
		// Potrzebny w klasie PPM do wpisania wartosci pobranych z pliku
		friend std::istream& operator>> (std::istream& is, Color&);

};

#endif