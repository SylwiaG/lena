#include "stdafx.h"
#include "ppm.h"
#include <fstream>

// Konstrukor bezparametrowy, wywoluje konstruktor nadklasy tj. Obraz
PPM::PPM() :Obraz() {}

// Konstruktor przyjmujacy jako parametr string
PPM::PPM(std::string fileName) : Obraz()
{
	loadImage(fileName);
}

// Konstruktor kopiujacy
PPM::PPM(const PPM& ppm) : Obraz(ppm) {}

// Destruktor
PPM::~PPM() {}

// Metoda wirtualna (abstrakcyjna), ktora pobiera zawartosc pliku PPM
void PPM::loadImage(std::string fileName)
{
	Color color(0, 0, 0);
	std::string eraser;

	// Otwiera plik o nazwie fileName do odczytu
	// Uzycie metody c_str daje mozliwosc przekazania tekstu do funkcji, ktora jako argument przyjmuje wartosc char *lub const char *
	std::ifstream file;
	file.open(fileName.c_str());

	// Upewniam sie, czy plik dziala i udalo sie go otworzyc
	if (file.good())
	{
		// Pobieram 'P3' w celu pominiecia
		getline(file, eraser);

		// Pobieram '# Created by IrfanView' w celu pominiecia
		getline(file, eraser);

		// Wczytuje do obrazu wymiary
		file >> x >> y;

		// Petla wykonuje sie, az do napotkania konca pliku
		while (!file.eof())
		{
			// Wczytuje wartosci z pliku do obiektu klasy kolor
			file >> color;

			// Dodaje uzupelniony obiekt do wektora
			wektorKolorow.push_back(color);
		}
	}
	
	// Zamykam uchwyt do pliku
	file.close();
}