#include "stdafx.h"
#include "obraz.h"

// Konstruktor bezparametrowy
Obraz::Obraz()
{
	this->x = 0;
	this->y = 0;
}

// Konstruktor kopiujacy
Obraz::Obraz(const Obraz& obraz)
{
	this->x = obraz.x;
	this->y = obraz.y;

	this->wektorKolorow = obraz.wektorKolorow;
}

// Destrukor
Obraz::~Obraz() {}

// Gettery
unsigned int Obraz::getX()
{
	return this->x;
}

unsigned int Obraz::getY()
{
	return this->y;
}

// Zwraca wektor obiektow klasy Color
const std::vector<Color>& Obraz::getWektorKolorow() const
{
	return wektorKolorow;
}

// Settery
void Obraz::setX(int x)
{
	this->x = x;
}

void Obraz::setY(int y)
{
	this->y = y;
}


