#ifndef OBRAZ_H
#define OBRAZ_H

#include "color.h"
#include <vector>

class Obraz
{
	protected:
		unsigned int x;
		unsigned int y;

		// Wektor
		// Pojemnik na obiekty klasy Color, taka dynamiczna tablica, ktora sama zwieksza swoja wielkosc wg. potrzeb
		// Uzylabym setu zeby nie powtarzac wartosci, ale podobno nie mozna :)
		// http://cpp0x.pl/dokumentacja/standard-C++/vector/819
		std::vector<Color> wektorKolorow;

	public:
		// Konstruktor bezparametrowy
		Obraz();

		// Konstruktor kopiujacy
		Obraz(const Obraz&);

		// Destrukor
		virtual ~Obraz();

		// Gettery
		unsigned int getX();
		unsigned int getY();
		const std::vector<Color>& getWektorKolorow() const;

		// Settery
		void setX(int x);
		void setY(int y);

		// Metoda wirtualna (abstrakcyjna)
		// Jesli taka metoda nie zostanie przesloniona, to klasa stanie sie klasa abstrakcyjna i nie bedzie mozna utworzyc jej obiektow
		// http://cpp0x.pl/kursy/Programowanie-obiektowe-C++/Polimorfizm/Metody-wirtualne/495
		virtual void loadImage(std::string) = 0;
};

#endif