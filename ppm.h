#ifndef PPM_H
#define PPM_H

#include "obraz.h"
#include <string>

// Dziediczenie publiczne. Skladowe publiczne klasy bazowej sa odziedziczone jako publiczne, a skladowe chronione jako chronione.
class PPM : public Obraz
{
	public:
		// Konstrukor bezparametrowy
		PPM();

		// Konstruktor przyjmujacy jako parametr string
		PPM(std::string);

		// Konstruktor kopiujacy
		PPM(const PPM&);

		// Destruktor
		~PPM();

		// Metoda wirtualna (abstrakcyjna)
		// Jesli taka metoda nie zostanie przesloniona, to klasa stanie sie klasa abstrakcyjna i nie bedzie mozna utworzyc jej obiektow
		// Metoda pobiera zawartosc pliku PPM
		// http://cpp0x.pl/kursy/Programowanie-obiektowe-C++/Polimorfizm/Metody-wirtualne/495
		virtual void loadImage(std::string);
};

#endif